$(document).ready(function() {
  $(window).scroll(function() {
      var wScroll = $(this).scrollTop();
      if(wScroll > $('.section-one').offset().top - $(window).height() ) {
        var sectionOne = $('.section-one').offset().top - $(window).height();
        $('.navbar-inverse').addClass('top-navbar-in');
        console.log("wScroll = "+ wScroll +' section one = ' + sectionOne);
      }
      if(wScroll > $('.section-two').offset().top - ($(window).height() - 400 )) {
          $('.navbar-inverse').removeClass('top-navbar-in');
          var test = $('.section-two').offset().top - ($(window).height() - 400 );
              console.log("test "+test);
          $('.caption-portfolio .img-content').each(function(i) {
              setTimeout(function() {
                $('.caption-portfolio .img-content').eq(i).css('opacity', '1');
                $('.caption-portfolio .img-content').eq(i).addClass('lightSpeedIn');
              }, 150 * (i+1));
          });
      }
  });
});
